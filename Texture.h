#ifndef TEXTURE_H_WGIYHCXP
#define TEXTURE_H_WGIYHCXP

#include <glad/glad.h>

#include <string>

class Texture
{
public:
	Texture(const std::string& imagePath, bool alpha);
	Texture(Texture&&) noexcept ;

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;
	Texture& operator=(Texture&&) = delete;

	virtual ~Texture();

	void bind(unsigned int i) const;
	void unBind() const;

private:
	GLuint m_ID;
};

#endif /* end of include guard: TEXTURE_H_WGIYHCXP */
