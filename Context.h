#ifndef WINDOW_H_GBYURNDV
#define WINDOW_H_GBYURNDV

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <string>

class Context
{
	public:
		Context(int width, int height, const std::string& title,
				bool debug = false);
		virtual ~Context(); 
		Context(const Context&) = delete;
		Context& operator=(const Context&) = delete;
		Context(Context&&) = delete;
		Context& operator=(Context&&) = delete;

		[[nodiscard]] GLFWwindow* getWindow() const { return m_window; }

		[[nodiscard]] int getWidth() const { return m_width; }
		[[nodiscard]] int getHeight() const { return m_height; }
		[[nodiscard]] float getAspectRatio() const { return static_cast<float>(m_width)/
															static_cast<float>(m_height); }

		std::pair<double, double> getMouseNormalCoordinate();

		void updateViewPort(int width, int height);
		[[nodiscard]] bool shouldClose() const;
		void refresh();

	private:
		static size_t m_count; //NOLINT
		GLFWwindow* m_window;
		int m_width;
		int m_height;
};

#endif /* end of include guard: WINDOW_H_GBYURNDV */
