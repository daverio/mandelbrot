#include <fstream>
#include <iostream>
#include <stdexcept>

#include "Shader.h"
#include "glm/gtc/type_ptr.hpp"

namespace
{
	std::string readShader(const std::string& shaderFilename)
	{
		std::ifstream shaderFile(shaderFilename);
		std::string shaderCode;

		if(shaderFile.good())
		{
			shaderCode = std::string(std::istreambuf_iterator<char>{shaderFile}, {});
		}
		else
		{
			throw std::runtime_error("Could not read \'" + shaderFilename + "\'." + "\n");
		}

		return shaderCode;
	}

	GLuint compileShader(GLenum type, const std::string& shaderCode, const std::string& shaderFilename)
	{
		const auto* pShaderCode{shaderCode.c_str()};
		auto shader{glCreateShader(type)}; 
		glShaderSource(shader, 1, &pShaderCode, nullptr);

		glCompileShader(shader);

		GLint success{0};
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if(success != GL_TRUE)
		{
			GLint length{0};
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
			GLchar* log{static_cast<GLchar*>(alloca(length*sizeof(GLchar)))};
			glGetShaderInfoLog(shader, length, nullptr, log);
			glDeleteShader(shader);

			throw std::runtime_error("In \'" + shaderFilename + "\': " + log + "\n");
		}
		return shader;
	}

	GLuint linkProgram(GLuint vertexShader, GLuint fragmentShader)
	{
		auto id{glCreateProgram()};
		glAttachShader(id, vertexShader);
		glAttachShader(id, fragmentShader);
		glLinkProgram(id);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		GLint success{0};
		glGetProgramiv(id, GL_LINK_STATUS, &success);
		if(success != GL_TRUE)
		{
			GLint length{0};
			glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);
			GLchar* log{static_cast<GLchar*>(alloca(length*sizeof(GLchar)))};
			glGetProgramInfoLog(id, length, nullptr, log);
			glDeleteProgram(id);

			throw std::runtime_error(log);
		}
		return id;
	}

	template<typename T>
	const T* value_ptr(const ComplexQuadrupple<T>& quadrupple)
	{
		return reinterpret_cast<const T*>(&quadrupple);//NOLINT
	}
}

Shader::Shader(const std::string& vertexShaderFilename,
		const std::string& fragmentShaderFilename)
{
	auto vertexShaderCode{readShader(vertexShaderFilename)};
	auto fragmentShaderCode{readShader(fragmentShaderFilename)};

	auto vertexShader{compileShader(GL_VERTEX_SHADER, vertexShaderCode, vertexShaderFilename)};
	auto fragmentShader{compileShader(GL_FRAGMENT_SHADER, fragmentShaderCode, fragmentShaderFilename)};
	
	m_ID = linkProgram(vertexShader, fragmentShader);
	glUseProgram(m_ID);
}

Shader::~Shader()
{
	glDeleteProgram(m_ID);
}

void Shader::setUniform(const std::string &name, GLfloat value)
{
	auto location{getUniformLocation(name)};
	glUniform1f(location, value);
}

void Shader::setUniform(const std::string &name, GLdouble value)
{
	auto location{getUniformLocation(name)};
	glUniform1d(location, value);
}

void Shader::setUniform(const std::string &name, GLuint value)
{
	auto location{getUniformLocation(name)};
	glUniform1ui(location, value);
}

void Shader::setUniform(const std::string &name, GLint value)
{
	auto location{getUniformLocation(name)};
	glUniform1i(location, value);
}

void Shader::setUniform(const std::string& name, const glm::vec2& vector)
{
	auto location{getUniformLocation(name)};
	glUniform2fv(location, 1, glm::value_ptr(vector));
}

void Shader::setUniform(const std::string& name, const glm::dvec2& vector)
{
	auto location{getUniformLocation(name)};
	glUniform2dv(location, 1, glm::value_ptr(vector));
}

void Shader::setUniform(const std::string &name, const glm::vec3 &vector)
{
	auto location{getUniformLocation(name)};
	glUniform3fv(location, 1, glm::value_ptr(vector));
}
void Shader::setUniform(const std::string& name, const glm::mat3& matrix)
{
	auto location{getUniformLocation(name)};
	glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniform(const std::string& name, const glm::mat4& matrix)
{
	auto location{getUniformLocation(name)};
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniform(const std::string& name, const PrecalcZs<GLfloat>& value)
{
	auto location{getUniformLocation(name)};
	const auto *address{glm::value_ptr(*value.data())};
	glUniform2fv(location, value.size(), address);
}

void Shader::setUniform(const std::string& name, const PrecalcZs<GLdouble>& value)
{
	auto location{getUniformLocation(name)};
	const auto *address{glm::value_ptr(*value.data())};
	glUniform2dv(location, value.size(), address);
}

void Shader::setUniform(const std::string& name, const TaylorCoeffs<GLfloat>& value)
{
	auto location{getUniformLocation(name)};
	const auto *address{value_ptr(*value.data())};
	glUniform4fv(location, value.size()*2, address);
}

void Shader::setUniform(const std::string& name, const TaylorCoeffs<GLdouble>& value)
{
	auto location{getUniformLocation(name)};
	const auto *address{value_ptr(*value.data())};
	glUniform4dv(location, value.size()*2, address);
}

GLint Shader::getUniformLocation(const std::string& name)
{
	auto location{m_uniformCache.try_emplace(name, 0)};

	if(location.second)
	{
		auto result (location.first->second = glGetUniformLocation(m_ID, name.c_str()));
#ifdef DEBUG
		if(result == -1)
		{
			std::cout << "Uniform \"" << name << "\" not found\n";
		}
#endif 
		return result;
	}
	return location.first->second;
}
