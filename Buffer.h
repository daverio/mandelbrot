#ifndef BUFFER_H_6AOGUCEN
#define BUFFER_H_6AOGUCEN

#include <glad/glad.h>

#include <vector>

class Buffer
{
	public:
		Buffer();
		virtual ~Buffer();
		Buffer(const Buffer&) = delete;
		Buffer& operator=(const Buffer&) = delete;
		Buffer(Buffer&&) = default;
		Buffer& operator=(Buffer&&) = default;

		template<typename T>
		Buffer(const std::vector<T>& data) : Buffer() { setData(data); }

		[[nodiscard]] GLsizei getCount() const { return m_count; }

		template<typename T>
		void setData(const std::vector<T>& data)
		{
			m_count = data.size();

			auto elementSize{sizeof(T)};
			auto size{elementSize*m_count};

			glNamedBufferData(m_ID, size, data.data(), GL_STATIC_DRAW);
		}

	protected:
		void bind(GLenum target) const;
		void unBind(GLenum target) const;

	private:
		GLuint m_ID;
		GLsizei m_count;
};

#endif /* end of include guard: BUFFER_H_6AOGUCEN */
