#include "IndexBuffer.h"

void IndexBuffer::bind()
{
	Buffer::bind(GL_ELEMENT_ARRAY_BUFFER);
}

void IndexBuffer::unBind()
{
	Buffer::bind(GL_ELEMENT_ARRAY_BUFFER);
}
