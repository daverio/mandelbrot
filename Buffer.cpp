#include "Buffer.h"

Buffer::Buffer()
	: m_ID{}, m_count{}
{
	glCreateBuffers(1, &m_ID);
}

Buffer::~Buffer()
{
	glDeleteBuffers(1, &m_ID);
}

void Buffer::bind(GLenum target) const
{
	glBindBuffer(target, m_ID);
}

void Buffer::unBind(GLenum target) const
{
	glBindBuffer(target, m_ID);
}
