#ifndef RENDERER_H_IV6MHB9G
#define RENDERER_H_IV6MHB9G

#include <glad/glad.h>
#include <glm/mat3x3.hpp>

#include <iostream>
#include <string>
#include <utility>

#include "Context.h"
#include "IndexBuffer.h"
#include "PrecisionFloat.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"
#include "VertexBuffer.h"


constexpr gpuFloat defaultScale{1.6};
constexpr std::tuple<double, double> defaultCenter{-0.7, 0};

constexpr unsigned int defaultMaxIterations{1000};
constexpr float defaultCyclePeriod{250};
constexpr float defaultLimitIterations{1};
constexpr float defaultColorDamping{4.5};

class Renderer
{
private:
	std::shared_ptr<Context> m_context;

	std::vector<Shader> m_shaders;
	std::vector<VertexArray> m_VAOs;
	std::vector<VertexBuffer> m_VBOs;
	std::vector<VertexBufferLayout> m_layouts;
	std::vector<IndexBuffer> m_IBOs;
	std::vector<Texture> m_textures;

	gpuFloat m_scale{defaultScale};
	PrecisionPoint m_center{defaultCenter};

	unsigned int m_maxIterations{defaultMaxIterations};
	float m_cyclePeriod{defaultCyclePeriod};
	float m_limitIterations{defaultLimitIterations};
	float m_colorDamping{defaultColorDamping};

public:
	Renderer(std::shared_ptr<Context> context);

	void draw();

	void setAspectRatio(float aspectRatio);
	void setCenter(const decltype(m_center)& center);
	void setScale(decltype(m_scale) scale);
	void setMaxIterations(decltype(m_maxIterations) maxIterations);
	void setCyclePeriod(decltype(m_cyclePeriod) cyclePeriod);
	void setLimitIterations(decltype(m_limitIterations) limitIterations);
	void setColorDamping(decltype(m_colorDamping) colorDamping);

	[[nodiscard]] auto getScale() const { return m_scale; }
	[[nodiscard]] auto getCenter() const { return m_center; }
	[[nodiscard]] auto getMaxIterations() const { return m_maxIterations; }
	[[nodiscard]] auto getCyclePeriod() const { return m_cyclePeriod; }
	[[nodiscard]] auto getLimitIterations() const { return m_limitIterations; }
	[[nodiscard]] auto getColorDamping() const { return m_colorDamping; }
};

#endif /* end of include guard: RENDERER_H_IV6MHB9G */
