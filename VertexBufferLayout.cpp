#include "VertexBufferLayout.h"

template<>
void VertexBufferLayout::addLayout<GLfloat>(GLint count)
{
	auto size{count * sizeof(GLfloat)};
	m_elements.push_back({count, GL_FLOAT, GL_FALSE, size});

	m_stride += size;
}

void VertexBufferLayout::buildLayout()
{
	std::size_t offset{0};
	for(std::size_t i{0}; i != m_elements.size(); i++)
	{
		glVertexAttribPointer(i, m_elements[i].count, m_elements[i].type,
						m_elements[i].normalized, m_stride, reinterpret_cast<void*>(offset));

		glEnableVertexAttribArray(i);
		offset += m_elements[i].size;
	}
}
