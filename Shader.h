#ifndef SHADER_H_Z8PDI3QF
#define SHADER_H_Z8PDI3QF

#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <complex>
#include <vector>
#include <unordered_map>
#include <string>

constexpr unsigned int maxPrecalcIterations{2000};

template<typename T>
struct ComplexQuadrupple
{
	std::complex<T> z, a, b, c;
};

template<typename T>
using TaylorCoeffs = std::vector<ComplexQuadrupple<T>>;

template<typename T>
using PrecalcZs = std::vector<glm::vec<2, T>>;

class Shader
{
public:
	Shader(const std::string& vertexShaderFilename,
			const std::string& fragmentShaderFilename);
	virtual ~Shader();
	Shader(const Shader&) = delete;
	Shader(Shader&&) = default;
	Shader& operator=(const Shader&) = delete;
	Shader& operator=(Shader&&) = default;

	void setUniform(const std::string& name, GLuint value);
	void setUniform(const std::string& name, GLint value);
	void setUniform(const std::string& name, GLfloat value);
	void setUniform(const std::string& name, GLdouble value);
	void setUniform(const std::string& name, const glm::vec2& vector);
	void setUniform(const std::string& name, const glm::dvec2& vector);
	void setUniform(const std::string& name, const glm::vec3& vector);
	void setUniform(const std::string& name, const glm::mat3& matrix);
	void setUniform(const std::string& name, const glm::mat4& matrix);
	void setUniform(const std::string& name,
									const PrecalcZs<GLfloat>& value);
	void setUniform(const std::string& name,
									const PrecalcZs<GLdouble>& value);
	void setUniform(const std::string& name,
									const TaylorCoeffs<GLfloat>& value);
	void setUniform(const std::string& name,
									const TaylorCoeffs<GLdouble>& value);

private:
	GLint getUniformLocation(const std::string& name);

	GLuint m_ID;
	std::unordered_map<std::string, GLint> m_uniformCache;
};

#endif /* end of include guard: SHADER_H_Z8PDI3QF */
