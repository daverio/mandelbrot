#version 450 core

in vec2 v_fragPosition;

out vec4 color;

uniform sampler1D u_tex;
uniform uint u_paletteResolution;

uniform float u_scale;
uniform vec2 u_center;
uniform vec2 u_mouse;

uniform uint u_maxIterations;
uniform float u_limitIterations;
uniform float u_cyclePeriod;
uniform float u_colorDamping;

const uint maxPrecalcIterations = 2000;
uniform vec2 u_zn[maxPrecalcIterations];

vec2 cMul(vec2 a, vec2 b)
{
	return vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

dvec2 cMul(dvec2 a, dvec2 b)
{
	return dvec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

float itToCoord(float it, float cyclePeriod, float damping)
{
	float coord = 1/damping * log(damping/cyclePeriod*it + 1);
	coord -= int(coord);
	return coord + 1/(2*float(u_paletteResolution));
}

vec4 itToColor(float it, uint maxIterations, float cyclePeriod, float limitIterations)
{
	float lastShownIteration = maxIterations - limitIterations;
	if(it <= lastShownIteration)
	{
		return texture(u_tex, itToCoord(it, cyclePeriod, u_colorDamping));
	}
	else
	{
		float lastCoord = itToCoord(lastShownIteration, cyclePeriod, u_colorDamping);
		vec4 lastShownColor = texture(u_tex, lastCoord);
		vec4 maxColor = texture(u_tex, 1 - 1/(2*float(u_paletteResolution)));
		
		return mix(maxColor, lastShownColor, (maxIterations - it)/limitIterations);
	}
}

float iterateMandelbrot(uint maxIterations)
{
	const float threshold = 64;

	dvec2 z0 = dvec2(u_center+u_scale*v_fragPosition);
	dvec2 z = z0;
	for(int i = 0; i<=maxIterations; i++)
	{
		float z2 = float(dot(z,z));
		if(z2 > threshold)
		{
			return i+1-log(log(z2)/(2*log(2)))/log(2);
		}
		z = cMul(z,z) + z0;
	}
	return maxIterations;
}

float iterateMandelbrotDelta(uint maxIterations)
{
	vec2 z0 = u_zn[0].xy;
	vec2 delta = u_scale*(v_fragPosition-u_mouse);
	vec2 delta0 = delta;

	const float threshold = 64;
	for(int i = 0; i<=maxIterations; i++)
	{
		vec2 x = z0 + delta;
		float x2 = dot(x,x);
		if(x2 > threshold)
		{
			return i+1-log(log(x2)/(2*log(2)))/log(2);
		}
		vec2 z = u_zn[i].xy;
		delta = 2.*cMul(z, delta) + cMul(delta, delta) + delta0;
	}
	return maxIterations;
}

/* int iterateMandelbrotDeltaTaylor(int maxIterations) */
/* { */
/* 	const double threshold = 4; */

/* 	dvec2 z0 = u_taylorCoeffs[0].xy; */
/* 	dvec2 delta = u_scale*v_fragPosition + u_center - z0; */
/* 	dvec2 delta2 = cMul(delta, delta); */
/* 	dvec2 delta3 = cMul(delta, delta2); */

/* 	int i = maxIterations/2; */
/* 	for(int dicho = (i+1)/2; ; dicho/=2) */
/* 	{ */
/* 		dvec2 z = u_taylorCoeffs[i*2].xy; */
/* 		dvec2 a = u_taylorCoeffs[i*2].zw; */
/* 		dvec2 b = u_taylorCoeffs[i*2+1].xy; */
/* 		dvec2 c = u_taylorCoeffs[i*2+1].zw; */

/* 		dvec2 result = z + cMul(a,delta) + cMul(b,delta2) + cMul(c,delta3); */
/* 		double result2 = dot(result,result); */
/* 		bool explode = result2 > threshold || isnan(result2) || isinf(result2); */

/* 		if(dicho == 0) */
/* 		{ */
/* 			if(!explode) */
/* 			{ */
/* 				return i+1; */
/* 			} */
/* 			return i; */
/* 		} */
/* 		if(explode) */
/* 		{ */
/* 			i -= dicho; */
/* 		} */
/* 		else */
/* 		{ */
/* 			i += dicho; */
/* 		} */
/* 	} */
/* } */

void main()
{
	float it = 0;
	if(max(abs(u_mouse.x), abs(u_mouse.y)) < 1)
	{
		it = iterateMandelbrotDelta(u_maxIterations);
	}
	else
	{
		it = iterateMandelbrot(u_maxIterations);
	}

	color = itToColor(it, u_maxIterations, u_cyclePeriod, u_limitIterations);
}
