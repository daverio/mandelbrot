#version 450 core

layout(location = 0) in vec2 position;

out vec2 v_fragPosition;

uniform float u_aspectRatio;

void main()
{
	gl_Position = vec4(position, 0, 1);
	v_fragPosition = vec2(u_aspectRatio*position.x, position.y);
}
