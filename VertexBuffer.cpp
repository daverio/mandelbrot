#include "VertexBuffer.h"

void VertexBuffer::bind()
{
	Buffer::bind(GL_ARRAY_BUFFER);
}

void VertexBuffer::unBind()
{
	Buffer::unBind(GL_ARRAY_BUFFER);
}
