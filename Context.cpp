#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <exception>
#include <stdexcept>
#include <iostream>

#include "Context.h"
#include "glDebugOutput.h"

size_t Context::m_count = 0; //NOLINT

Context::Context(int width, int height, const std::string& title, bool debug)
	: m_width(width), m_height(height)
{
	glfwSetErrorCallback([](int /*unused*/, const char* error)
			{ throw std::runtime_error(error); });

	if(m_count==0 && (glfwInit() == 0))
	{
		throw std::runtime_error("glfwInit failed.");
	}

	constexpr auto major_version{4};
	constexpr auto minor_version{5};
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, static_cast<int>(debug));
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major_version);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor_version);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	m_window = glfwCreateWindow(width, height, title.c_str(),
							nullptr, nullptr);

	if(m_window == nullptr)
	{
		glfwTerminate();
		throw std::runtime_error("Could not create GLFWwindow");
	}

	if(m_count==0)
	{
		glfwMakeContextCurrent(m_window);

		if(gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) //NOLINT
		{
			throw std::runtime_error("Could not initiate GLEW.");
		}

		if(debug)
		{
			int flags{0}; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
			if((flags & GL_CONTEXT_FLAG_DEBUG_BIT) != 0)
			{
				glEnable(GL_DEBUG_OUTPUT);
				glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
				glDebugMessageCallback(glDebugOutput,nullptr);
				glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
						0, nullptr, GL_TRUE);
			}
			else
			{
				throw std::runtime_error("Could not enable debug context.");
			}
		}
	}

	glfwSwapInterval(0);

	/* glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); */

	m_count++;
}

Context::~Context()
{
	if(m_count==1)
	{
		glfwTerminate();
	}
	m_count--;
}

bool Context::shouldClose() const
{
	return glfwWindowShouldClose(m_window) != 0;
}

void Context::refresh()
{
	glfwSwapBuffers(m_window);
	glfwPollEvents();
}

void Context::updateViewPort(int width, int height)
{
	m_width = width;
	m_height = height;
	glViewport(0, 0, width, height);
}

std::pair<double, double> Context::getMouseNormalCoordinate()
{
	double xpos{};
	double ypos{};
	glfwGetCursorPos(m_window, &xpos, &ypos);

	auto min{std::min(m_width, m_height)};
	auto xMouseCoord{(2*xpos - m_width)/min};
	auto yMouseCoord{-(2*ypos - m_height)/min};

	return {xMouseCoord, yMouseCoord};
}
