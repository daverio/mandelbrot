#include <string>

#include "App.h"

int main()
{
	constexpr int width{720};
	constexpr int height{720};
	std::string title{"OpenGL Tutorial"};

#ifdef DEBUG
	bool debug{true};
#else
	bool debug{false};
#endif

	App app(width, height, title, debug);

	return app.run();
}
