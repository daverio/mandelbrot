#include <gmp.h>
#include <gmpxx.h>

struct Precision
{
	Precision(mp_bitcnt_t N) { mpf_set_default_prec(N); }
};

class PrecisionFloat : public mpf_class
{
	public:
		using mpf_class::mpf_class;
};

static Precision precision{128};
using gpuFloat = float;
using precisionFloat = PrecisionFloat;

struct PrecisionPoint
{
	precisionFloat x; //NOLINT
	precisionFloat y; //NOLINT

	PrecisionPoint(precisionFloat x, precisionFloat y)
		: x(std::move(x)), y(std::move(y)) {}
	PrecisionPoint(std::tuple<precisionFloat, precisionFloat> p)
	{
		auto [x,y]{std::move(p)};
		this->x = x;
		this->y = y;
	}

	PrecisionPoint& operator+=(std::tuple<double, double> p)
	{
		auto [x,y]{p};
		this->x += x;
		this->y += y;
		return *this;
	}
};
