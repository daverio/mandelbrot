#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>

#include <complex>
#include <fstream>
#include <iostream>

#include "Constants.h"
#include "Renderer.h"
#include "vendor/include/glm/ext/matrix_transform.hpp"

namespace
{
	void setPalette(Shader& shader)
	{
		GLuint id{0};
		glCreateTextures(GL_TEXTURE_1D, 1, &id);
		glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_REPEAT);

		int width{0};
		int height{0};
		int nrChannels{0};
		stbi_set_flip_vertically_on_load(1);  
		GLubyte *data{stbi_load("res/mandelbrotpal.png", &width, &height, &nrChannels, 0)};
		if(data != nullptr)
		{
			glBindTextureUnit(0, id);
			glBindTexture(GL_TEXTURE_1D, id);
			glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		}
		shader.setUniform("u_tex", 0);
		shader.setUniform("u_paletteResolution", static_cast<unsigned int>(width));
	}

}
Renderer::Renderer(std::shared_ptr<Context> context)
	: m_context(std::move(context))
{
	m_VAOs.emplace_back();
	m_VBOs.emplace_back(std::vector<GLfloat>({-1, -1,  1, -1,  1, 1,  -1, 1}));
	m_VBOs[0].bind();
	m_IBOs.emplace_back(std::vector<GLuint>({0, 1, 2, 2, 3, 0}));
	m_IBOs[0].bind();
	m_layouts.emplace_back();
	m_layouts[0].addLayout<GLfloat>(2);
	m_layouts[0].buildLayout();
	m_shaders.emplace_back(std::string("res/shader/shader.vert"), std::string("res/shader/shader.frag"));
	setPalette(m_shaders[0]);

	this->setAspectRatio(1);
	this->setScale(m_scale);
	this->setCenter(m_center);
	this->setMaxIterations(m_maxIterations);
	this->setCyclePeriod(m_cyclePeriod);
	this->setLimitIterations(m_limitIterations);
	this->setColorDamping(m_colorDamping);
}

void Renderer::draw()
{
	auto[xMouse, yMouse]{m_context->getMouseNormalCoordinate()};
	m_shaders[0].setUniform("u_mouse", glm::vec2(xMouse, yMouse));

	PrecalcZs<gpuFloat> zn(m_maxIterations);

	precisionFloat z0x{m_center.x + m_scale*xMouse};
	std::complex<precisionFloat> z0{m_center.x + m_scale*xMouse,
							m_center.y + m_scale*yMouse};
	auto z{z0};

	for(std::size_t i{0}; i!=zn.size(); i++)
	{
		zn[i] = {z.real().get_d(), z.imag().get_d()};
		z = z*z + z0;
	}
	m_shaders[0].setUniform("u_zn", zn);
	
	glClear(GL_COLOR_BUFFER_BIT);

	glDrawElements(GL_TRIANGLES, m_IBOs[0].getCount(), GL_UNSIGNED_INT, nullptr);
}

void Renderer::setAspectRatio(float aspectRatio)
{
	m_shaders[0].setUniform("u_aspectRatio", aspectRatio);
}

void Renderer::setCenter(const decltype(m_center)& center)
{
	auto x{center.x.get_d()};
	auto y{center.y.get_d()};
	m_shaders[0].setUniform("u_center", glm::vec<2, gpuFloat>{x,y});
	m_center = center;
}

void Renderer::setScale(decltype(m_scale) scale)
{
	if(scale >= 0)
	{
		m_shaders[0].setUniform("u_scale", scale);
		m_scale = scale;
		std::cout << "Scale: " << scale << std::endl;
	}
}

void Renderer::setMaxIterations(decltype(m_maxIterations) maxIterations)
{
	if(maxIterations >= 0)
	{
		m_shaders[0].setUniform("u_maxIterations", maxIterations);
		m_maxIterations = maxIterations;
		std::cout << "Max iterations: " << maxIterations << std::endl;
	}
}

void Renderer::setCyclePeriod(decltype(m_cyclePeriod) cyclePeriod)
{
	if(cyclePeriod >= 0)
	{
		m_shaders[0].setUniform("u_cyclePeriod", cyclePeriod);
		m_cyclePeriod = cyclePeriod;
		std::cout << "Cycle period : " << cyclePeriod << std::endl;
	}
}

void Renderer::setColorDamping(decltype(m_colorDamping) colorDamping)
{
	if(colorDamping >= 0)
	{
		m_shaders[0].setUniform("u_colorDamping", colorDamping);
		m_colorDamping = colorDamping;
		std::cout << "Color damping: " << colorDamping << std::endl;
	}
}

void Renderer::setLimitIterations(decltype(m_limitIterations) limitIterations)
{
	if(limitIterations >= 0)
	{
		m_shaders[0].setUniform("u_limitIterations", limitIterations);
		m_limitIterations = limitIterations;
		std::cout << "Limit iterations: " << limitIterations << std::endl;
	}
}
