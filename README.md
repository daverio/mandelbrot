# Mandelbrot renderer

This is my attempt to create a real time Mandelbrot renderer using OpenGL.

## Dependency

You'll need to have the headers and the binary of GLFW3, and have an OpenGL 4.5 compatible driver. The headers of all other libraries are included in this repository. The source code of libraries is included.

The Makefile is working for Linux and Windows.

## Building

There are to build targets: `debug` and `release`. The default target is `debug`.

Type
```
make [debug/release]
```
in the main directory. This will output object and dependency files as well as the executable in `build/[Linux/Windows]/[debug/release]`.
