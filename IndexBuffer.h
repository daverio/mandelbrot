#ifndef INDEXBUFFER_H_X9ACDUZY
#define INDEXBUFFER_H_X9ACDUZY

#include <glad/glad.h>

#include "Buffer.h"

class IndexBuffer : public Buffer
{
public:
	using Buffer::Buffer;
	void bind();
	void unBind();
};

#endif /* end of include guard: INDEXBUFFER_H_X9ACDUZY */
