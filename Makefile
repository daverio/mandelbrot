CXX = LANG=EN g++
CXXFLAGS = -Wall -std=c++17
SRCS = $(wildcard *.cpp)
OBJS = $(patsubst %.cpp, $(BUILDPATH)/%.o, $(SRCS))
DEPS = $(patsubst %.cpp, $(BUILDPATH)/%.d, $(SRCS))
VENDORSRCS = $(wildcard vendor/src/*.cpp)
VENDOROBJS = $(patsubst vendor/src/%.cpp, $(BUILDPATH)/%.o, $(VENDORSRCS))
CVENDORSRCS = $(wildcard vendor/src/*.c)
CVENDOROBJS = $(patsubst vendor/src/%.c, $(BUILDPATH)/%.o, $(CVENDORSRCS))
INCLUDE = -Ivendor/include
BUILDPATHROOT = build

ifneq ($(MAKECMDGOALS),clean)
ifeq ($(OS),Windows)
BUILDPATHSYSROOT = $(BUILDPATHROOT)/Windows
OUT = Mandelbrot.exe
LDFLAGS = -lopengl32 -lglfw3 -lgmp -lgmpxx
ifeq ($(MAKECMDGOALS),release)
LDFLAGS += -lgdi32 -static
endif
else
BUILDPATHSYSROOT = $(BUILDPATHROOT)/Linux
OUT = Mandelbrot
LDFLAGS = -lGL -lglfw -ldl -lgmp -lgmpxx
endif
endif

ifeq ($(MAKECMDGOALS),)
CXXFLAGS += -g3 -DDEBUG
BUILDPATH = $(BUILDPATHSYSROOT)/debug
endif
ifeq ($(MAKECMDGOALS),debug)
CXXFLAGS += -g3 -DDEBUG
BUILDPATH = $(BUILDPATHSYSROOT)/debug
endif
ifeq ($(MAKECMDGOALS),release)
CXXFLAGS += -O3
BUILDPATH = $(BUILDPATHSYSROOT)/release
endif

debug: $(BUILDPATH)/$(OUT)

release: $(BUILDPATH)/$(OUT)

$(BUILDPATH):
	mkdir -p $(BUILDPATH)

$(CVENDOROBJS) : $(BUILDPATH)/%.o: vendor/src/%.c | $(BUILDPATH)
	gcc -O3 $(INCLUDE) -c $< -o $@
$(VENDOROBJS) : $(BUILDPATH)/%.o: vendor/src/%.cpp | $(BUILDPATH)
	g++ -O3 $(INCLUDE) -c $< -o $@

$(OBJS): $(BUILDPATH)/%.o: %.cpp | $(BUILDPATH)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -MMD -MP -c $< -o $@

$(BUILDPATH)/$(OUT): $(VENDOROBJS) $(CVENDOROBJS) $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $(CVENDOROBJS) $(VENDOROBJS) $(OBJS) $(LDFLAGS)

-include $(DEPS)

.PHONY: clean
clean:
	$(RM) -r $(BUILDPATHROOT)
