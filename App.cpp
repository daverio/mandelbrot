#include "App.h"
#include "Constants.h"

#include <GLFW/glfw3.h>

#include <algorithm>
#include <iostream>
#include <numeric>

//ugly global variables for C-style callbacks (for GLFW)
static bool resetFPS{false}, showFPS{false}; //NOLINT
static App* currentApp{nullptr}; //NOLINT
static int swapInterval{1}; //NOLINT

void App::keyCallback(GLFWwindow*  /*window*/, int key, int /*unused*/, int action, int /*unused*/)
{
	if(key == GLFW_KEY_T && action == GLFW_PRESS)
	{
		glfwSwapInterval(swapInterval);
		resetFPS = true;
		swapInterval = 1 - swapInterval;
	}
	if(key == GLFW_KEY_F3 && action == GLFW_PRESS)
	{
		showFPS = !showFPS;
		resetFPS = true;
	}
}

void App::zoomCallback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
	auto& context{currentApp->m_context};
	auto& renderer{currentApp->m_renderer};

	auto scale{(renderer.getScale())};
	constexpr decltype(scale) scalespeed{0.95};
	auto dScale{std::pow(scalespeed,
						static_cast<decltype(scale)>(yoffset))};

	scale *= dScale;
	currentApp->m_renderer.setScale(scale);

	auto [xMouse, yMouse]{context->getMouseNormalCoordinate()};
	auto center{renderer.getCenter()};
	std::tuple dCenter{(1/dScale-1)*scale*xMouse, (1/dScale-1)*scale*yMouse};
	center += dCenter;

	currentApp->m_renderer.setCenter(center);
}

void App::resizeCallback(GLFWwindow* /*unused*/, int width, int height)
{
	currentApp->m_context->updateViewPort(width, height);
	auto aspectRatio{currentApp->m_context->getAspectRatio()};
	currentApp->m_renderer.setAspectRatio(aspectRatio);
}

App::App(int width, int height, std::string title, bool debug)
	  : m_context(std::make_shared<Context>(width, height, title, debug)),
		m_renderer(m_context)
{
	glfwSwapInterval(swapInterval);

	currentApp = this;
	glfwSetKeyCallback(m_context->getWindow(), App::keyCallback);
	glfwSetScrollCallback(m_context->getWindow(), App::zoomCallback);
	glfwSetFramebufferSizeCallback(m_context->getWindow(),
													App::resizeCallback);
}

void printFPS(float deltaTime, float percent = 1)
{

	constexpr float defaultDeltaTime{1/60.0F};
	constexpr size_t maxFrameNumber{0x4000};
	constexpr float updatePeriod{1.5};

	static std::vector<float> frameDurations(maxFrameNumber);
	static size_t index{0};
	static float averageDeltaTime{defaultDeltaTime};

	if(resetFPS)
	{
		index = 0;
		averageDeltaTime = defaultDeltaTime;
		resetFPS = false;
	}

	frameDurations[index] = deltaTime;
	index++;

	if(index*averageDeltaTime > updatePeriod || index == frameDurations.size())
	{
		auto last{frameDurations.begin() + index};

		constexpr float hundredPercent{100.0F};
		std::sort(frameDurations.begin(), last);
		auto max{frameDurations[index*(hundredPercent-percent)/hundredPercent]};
		auto min{frameDurations[index*percent/hundredPercent]};
		averageDeltaTime = std::accumulate(frameDurations.begin(), last,
			static_cast<decltype(*frameDurations.cbegin()) >(0)) / index;

		std::cout << "\nabs max: " << 1/frameDurations[0]
			<< "\n" << percent << "% max:  " << 1/min
			<< "\nabs min: " << 1/frameDurations[index-1]
			<< "\n" << percent << "% min:  " << 1/max
			<< "\navg:     " << 1/averageDeltaTime << std::endl;

		index = 0;
	}
}

void App::update(float deltaTime)
{
	if(showFPS)
	{
		printFPS(deltaTime);
	}

	static auto cyclePeriod{
		static_cast<double>(m_renderer.getCyclePeriod())};
	static auto limitIterations{
		static_cast<double>(m_renderer.getLimitIterations())};
	static auto maxIterations{
		static_cast<double>(m_renderer.getMaxIterations())};
	static auto colorDamping{
		static_cast<double>(m_renderer.getColorDamping())
	};
	double scale{m_renderer.getScale()};

	constexpr double speed{1};
	constexpr double magicDampingConstant{0.5};
	const double factor{1+speed*deltaTime/(1-magicDampingConstant*std::log(scale))};

	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_J) == GLFW_PRESS)
	{
		maxIterations *= factor;
		m_renderer.setMaxIterations(static_cast<unsigned int>(maxIterations));
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_K) == GLFW_PRESS)
	{
		maxIterations /= factor;
		m_renderer.setMaxIterations(static_cast<unsigned int>(maxIterations));
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_H) == GLFW_PRESS)
	{
		cyclePeriod *= factor;
		m_renderer.setCyclePeriod(cyclePeriod);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_L) == GLFW_PRESS)
	{
		cyclePeriod /= factor;
		m_renderer.setCyclePeriod(cyclePeriod);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_I) == GLFW_PRESS)
	{
		limitIterations *= factor;
		m_renderer.setLimitIterations(limitIterations);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_O) == GLFW_PRESS)
	{
		limitIterations /= factor;
		m_renderer.setLimitIterations(limitIterations);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_B) == GLFW_PRESS)
	{
		colorDamping *= factor;
		m_renderer.setColorDamping(colorDamping);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_N) == GLFW_PRESS)
	{
		colorDamping /= factor;
		m_renderer.setColorDamping(colorDamping);
	}
}

namespace
{
	float getDeltaTime()
	{

		constexpr float defaultDeltaTime{1/60.0F};

		float currentTime{static_cast<float>(glfwGetTime())};
		static float previousTime{currentTime - defaultDeltaTime};
		float deltaTime{currentTime - previousTime};
		previousTime = currentTime;

		return deltaTime;
	}
}

int App::run()
{

	while(!m_context->shouldClose())
	{
		update(getDeltaTime());

		m_renderer.draw();
		m_context->refresh();
	}

	return 0;
}
